<?php

namespace App\Security\Voter;

use App\Entity\User;
use App\Repository\ConversationRepository;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class UserVoter extends Voter
{
    public const VIEW = 'user_view';

    private const AUTHORIZATIONS = [
        self::VIEW,
    ];

    private ConversationRepository $conversationRepository;

    public function __construct(ConversationRepository $conversationRepository)
    {
        $this->conversationRepository = $conversationRepository;
    }

    /**
     * @param string $attribute
     * @param mixed  $subject
     *
     * @return bool
     */
    protected function supports($attribute, $subject): bool
    {
        return in_array($attribute, self::AUTHORIZATIONS)
            && $subject instanceof User;
    }

    /**
     * @param string         $attribute
     * @param mixed          $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        if (!$user instanceof UserInterface)
        {
            return false;
        }

        switch ($attribute)
        {
            case self::VIEW:
                return $this->canView($subject, $user);
        }

        return false;
    }

    private function canView(User $user, UserInterface $me): bool
    {
        if ($user->getId() === $me->getId()) {
            return true;
        }

        return $this->conversationRepository->findByUsers($user, $me) !== null;
    }
}
