<?php

namespace App\Security\Voter;

use App\Entity\Conversation;
use App\Entity\Participant;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class ConversationVoter extends Voter
{
    public const OPEN = 'conversation_open';
    public const ADD_MESSAGE = 'conversation_add_message';

    private const AUTHORIZATIONS = [
        self::OPEN,
        self::ADD_MESSAGE,
    ];

    /**
     * @param string $attribute
     * @param mixed  $subject
     *
     * @return bool
     */
    protected function supports($attribute, $subject): bool
    {
        return in_array($attribute, self::AUTHORIZATIONS)
            && $subject instanceof Conversation;
    }

    /**
     * @param string         $attribute
     * @param mixed          $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        if (!$user instanceof UserInterface)
        {
            return false;
        }

        switch ($attribute)
        {
            case self::OPEN:
            case self::ADD_MESSAGE:
                return $this->canOpen($subject, $user);
        }

        return false;
    }

    private function canOpen(Conversation $conversation, User $user): bool
    {
        /** @var Participant $participant */
        foreach ($conversation->getParticipants() as $participant) {
            if ($participant->getUser()->getId() === $user->getId()) {
                return true;
            }
         }

         return false;
    }
}
