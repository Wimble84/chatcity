<?php

namespace App\DataFixtures;

use App\Entity\Conversation;
use App\Entity\Message;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class MessageFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        /* For conversation 1 */

        /** @var Conversation $conversation */
        $conversation = $this->getReference(ConversationFixtures::CONVERSATION_1);
        $user1 = $this->getReference(UserFixtures::USER_1);
        $user2 = $this->getReference(UserFixtures::USER_2);

        for ($i=0, $j=10; $i<10; $i++, $j--) {
            $message = (new Message())
                ->setConversation($conversation)
                ->setContent("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.")
                ->setCreatedAt((new \DateTime())->modify('-' . $j . ' minute'));

            if ($i % 2 === 0) {
                $message->setAuthor($user1);
            } else {
                $message->setAuthor($user2);
            }

            $manager->persist($message);
        }

        $conversation->setLastMessage($message);
        $manager->persist($conversation);

        /* For conversation 2 */

        /** @var Conversation $conversation */
        $conversation2 = $this->getReference(ConversationFixtures::CONVERSATION_2);
        $user3 = $this->getReference(UserFixtures::USER_3);

        for ($i=0, $j=10; $i<10; $i++, $j--) {
            $message = (new Message())
                ->setConversation($conversation2)
                ->setContent("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.")
                ->setCreatedAt((new \DateTime())->modify('-' . $j . ' minute'));

            if ($i % 2 === 0) {
                $message->setAuthor($user1);
            } else {
                $message->setAuthor($user3);
            }

            $manager->persist($message);
        }

        $conversation2->setLastMessage($message);
        $manager->persist($conversation2);

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            UserFixtures::class,
            ConversationFixtures::class,
            ParticipantFixtures::class,
        ];
    }
}