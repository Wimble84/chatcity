<?php

namespace App\DataFixtures;

use App\Entity\Participant;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ParticipantFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /* For conversation 1 */
        $participant1 = (new Participant())
            ->setConversation($this->getReference(ConversationFixtures::CONVERSATION_1))
            ->setUser($this->getReference(UserFixtures::USER_1));
        $manager->persist($participant1);

        $participant2 = (new Participant())
            ->setConversation($this->getReference(ConversationFixtures::CONVERSATION_1))
            ->setUser($this->getReference(UserFixtures::USER_2));
        $manager->persist($participant2);

        /* For conversation 2 */

        $participant3 = (new Participant())
            ->setConversation($this->getReference(ConversationFixtures::CONVERSATION_2))
            ->setUser($this->getReference(UserFixtures::USER_1));
        $manager->persist($participant3);

        $participant4 = (new Participant())
            ->setConversation($this->getReference(ConversationFixtures::CONVERSATION_2))
            ->setUser($this->getReference(UserFixtures::USER_3));
        $manager->persist($participant4);

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            UserFixtures::class,
            ConversationFixtures::class,
        ];
    }
}