<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    public const USER_1 = 'user1';
    public const USER_2 = 'user2';
    public const USER_3 = 'user3';

    public function load(ObjectManager $manager): void
    {
        $user1 = (new User())
            ->setUsername('pierreb')
            ->setEmail('pierre@mail.fr')
            ->setFirstname('Pierre')
            ->setLastname('Brunel')
            ->setPassword('pierreb')
            ->setRoles(['ROLE_USER']);
        $manager->persist($user1);

        $user2 = (new User())
            ->setUsername('sophieb')
            ->setEmail('sophie@mail.fr')
            ->setFirstname('Sophie')
            ->setLastname('Brunel')
            ->setPassword('sophieb')
            ->setRoles(['ROLE_USER']);
        $manager->persist($user2);

        $user3 = (new User())
            ->setUsername('claireg')
            ->setEmail('claire@mail.fr')
            ->setFirstname('Claire')
            ->setLastname('Guichard')
            ->setPassword('claireg')
            ->setRoles(['ROLE_USER']);
        $manager->persist($user3);

        $user4 = (new User())
            ->setUsername('anneb')
            ->setEmail('anne@mail.fr')
            ->setFirstname('Anne')
            ->setLastname('Brunel')
            ->setPassword('anneb')
            ->setRoles(['ROLE_USER']);
        $manager->persist($user4);

        $manager->flush();

        $this->addReference(self::USER_1, $user1);
        $this->addReference(self::USER_2, $user2);
        $this->addReference(self::USER_3, $user3);
    }
}
