<?php

namespace App\DataFixtures;

use App\Entity\Conversation;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ConversationFixtures extends Fixture implements DependentFixtureInterface
{
    public const CONVERSATION_1 = 'conversation1';
    public const CONVERSATION_2 = 'conversation2';

    public function load(ObjectManager $manager): void
    {
        $conversation1 = (new Conversation());
        $manager->persist($conversation1);

        $conversation2 = (new Conversation());
        $manager->persist($conversation2);


        $manager->flush();

        $this->addReference(self::CONVERSATION_1, $conversation1);
        $this->addReference(self::CONVERSATION_2, $conversation2);
    }

    public function getDependencies(): array
    {
        return [
            UserFixtures::class,
        ];
    }
}