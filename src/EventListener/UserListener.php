<?php

namespace App\EventListener;

use App\Entity\User;
use App\Manager\UserManager;
use App\Service\Mailer;

class UserListener
{
    private UserManager $userManager;
    private Mailer $mailer;

    public function __construct(UserManager $userManager, Mailer $mailer)
    {
        $this->userManager = $userManager;
        $this->mailer = $mailer;
    }

    public function prePersist(User $user): void
    {
        $this->mailer->userCreation($user);
        $user->setPassword($this->userManager->hashPassword($user));
    }

}