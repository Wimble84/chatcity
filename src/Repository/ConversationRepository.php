<?php

namespace App\Repository;

use App\Entity\Conversation;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;
use function Doctrine\ORM\QueryBuilder;

/**
 * @method Conversation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Conversation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Conversation[]    findAll()
 * @method Conversation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConversationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Conversation::class);
    }

    public function findByUser(User $user): array
    {
        $qb = $this->createQueryBuilder('c');
        $qb
            ->select([
                'otherUser.username',
                'otherUser.id as otherUserId',
                'c.id AS conversationId',
                'lm.content AS lastMessageContent',
                'lm.createdAt AS lastMessageCreationDate'
            ])
            ->innerJoin('c.participants', 'p', Join::WITH, $qb->expr()->neq('p.user', ':user'))
            ->innerJoin('c.participants', 'me', Join::WITH, $qb->expr()->eq('me.user', ':user'))
            ->leftJoin('c.lastMessage', 'lm')
            ->innerJoin('me.user', 'meUser')
            ->innerJoin('p.user', 'otherUser')
            ->where('meUser.id = :user')
            ->setParameter('user', $user)
            ->orderBy('lm.createdAt', 'DESC');

        return $qb->getQuery()->getResult();
    }

    public function findByUsers(User $user1, User $user2): ?Conversation
    {
        $qb = $this->createQueryBuilder('c');
        $qb
            ->innerJoin('c.participants', 'p1')
            ->innerJoin('c.participants', 'p2')
            ->where($qb->expr()->eq('p1.user', ':user1'))
            ->andWhere($qb->expr()->eq('p2.user', ':user2'))
            ->setParameters([
                'user1' => $user1,
                'user2' => $user2,
            ]);

        return $qb->getQuery()->getOneOrNullResult();
    }
}
