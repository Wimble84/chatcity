<?php

namespace App\Repository;

use App\Entity\Conversation;
use App\Entity\Participant;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use function Doctrine\ORM\QueryBuilder;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(PasswordAuthenticatedUserInterface $user, string $newHashedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newHashedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    public function findByUsername(string $username): ?User
    {
        $qb = $this->createQueryBuilder('u');
        $qb
            ->where($qb->expr()->like('u.username', ':username'))
            ->setParameter('username', $username);

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function findConversationOtherParticipantUser(Conversation $conversation, User $user): ?User
    {
        $qb = $this->createQueryBuilder('u');
        $qb
            ->innerJoin(Participant::class, 'p', Join::WITH, $qb->expr()->eq('u', 'p.user'))
            ->innerJoin('p.conversation', 'c')
            ->where($qb->expr()->neq('p.user', ':user'))
            ->andWhere($qb->expr()->eq('p.conversation', ':conversation'))
            ->setParameters([
                'user' => $user,
                'conversation' => $conversation,
            ])
        ;

        return $qb->getQuery()->getOneOrNullResult();
    }
}
