<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ChangePassword extends Constraint
{
    public string $messageEquality = 'Le nouveau mot de passe doit être différent de l\'actuel.';
    public string $messageInvalid = 'Le mot de passe actuel est incorrect.';

    /**
     * @return string
     */
    public function getTargets(): string
    {
        return self::CLASS_CONSTRAINT;
    }

}
