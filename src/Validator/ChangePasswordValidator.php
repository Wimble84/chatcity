<?php

namespace App\Validator;

use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ChangePasswordValidator extends ConstraintValidator
{
    private UserPasswordHasherInterface $passwordHasher;
    private Security $security;

    public function __construct(UserPasswordHasherInterface $passwordHasher, Security $security)
    {
        $this->passwordHasher = $passwordHasher;
        $this->security = $security;
    }

    public function validate($data, Constraint $constraint): void
    {
        if ($data['current_password'] === $data['new_password'])
        {
            $this->context
                ->buildViolation($constraint->messageEquality)
                ->addViolation();
        }

        $isPasswordValid = $this->passwordHasher->isPasswordValid(
            $this->security->getUser(),
            $data['current_password']
        );

        if (!$isPasswordValid) {
            $this->context
                ->buildViolation($constraint->messageInvalid)
                ->addViolation();

        }
    }
}
