<?php

namespace App\Manager;

use App\Entity\Participant;
use Doctrine\ORM\EntityManagerInterface;

class ParticipantManager
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function save(Participant $participant, bool $andFlush = true): void
    {
        $this->entityManager->persist($participant);

        if ($andFlush) {
            $this->entityManager->flush();
        }
    }
}