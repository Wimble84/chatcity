<?php

namespace App\Manager;

use App\Entity\Conversation;
use App\Entity\Message;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class MessageManager
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function save(Message $user, bool $andFlush = true): void
    {
        $this->entityManager->persist($user);

        if ($andFlush) {
            $this->entityManager->flush();
        }
    }

    public function create(
        Conversation $conversation,
        User $currentUser,
        string $messageContent,
        bool $andFlush = true
    ): Message {
        $message = (new Message())
            ->setAuthor($currentUser)
            ->setContent($messageContent)
            ->setConversation($conversation);

        $this->save($message, $andFlush);

        return $message;
    }
}