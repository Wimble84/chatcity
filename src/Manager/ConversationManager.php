<?php

namespace App\Manager;

use App\Entity\Conversation;
use App\Entity\Participant;
use App\Entity\User;
use App\Repository\ConversationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;

class ConversationManager
{
    private EntityManagerInterface $entityManager;
    private Security $security;
    private ConversationRepository $conversationRepository;
    private ParticipantManager $participantManager;

    public function __construct(
        EntityManagerInterface $entityManager,
        Security $security,
        ConversationRepository $conversationRepository,
        ParticipantManager $participantManager
    ) {
        $this->entityManager = $entityManager;
        $this->security = $security;
        $this->conversationRepository = $conversationRepository;
        $this->participantManager = $participantManager;
    }

    public function save(Conversation $user, bool $andFlush = true): void
    {
        $this->entityManager->persist($user);

        if ($andFlush) {
            $this->entityManager->flush();
        }
    }

    public function isConversationAlreadyExists(User $user): bool
    {
        return $this->conversationRepository->findByUsers($this->security->getUser(), $user) !== null;
    }

    public function create(User $user, bool $andFlush = true): Conversation
    {
        $participant1 = new Participant();
        $participant1->setUser($user);

        $participant2 = new Participant();
        $participant2->setUser($this->security->getUser());

        $conversation = new Conversation();
        $conversation->addParticipant($participant1);
        $conversation->addParticipant($participant2);

        $this->participantManager->save($participant1, false);
        $this->participantManager->save($participant2, false);
        $this->save($conversation, $andFlush);

        $this->entityManager->refresh($conversation);

        return $conversation;
    }
}