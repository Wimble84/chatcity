<?php

namespace App\Controller\User;

use App\Repository\ConversationRepository;
use App\Repository\MessageRepository;
use App\Service\MercureJwtProvider;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AppController extends AbstractController
{
    /**
     * @Route("/", name="user-index")
     */
    public function index(
        ConversationRepository $conversationRepository,
        MessageRepository $messageRepository,
        MercureJwtProvider $jwtProvider
    ): Response {
        $conversations = $conversationRepository->findByUser($this->getUser());
        $conversationId = null;
        $otherUserId = null;

        $messages = [];
        if (!empty($conversations)) {
            $conversationId = $conversations[0]['conversationId'];
            $messages = $messageRepository->findByConversation(
                $conversationId,
                15
            );

            $otherUserId = $conversations[0]['otherUserId'];
        }

        return $this->render('user/index.html.twig', [
            'conversations' => $conversations,
            'messages' => $messages,
            'conversationId' => $conversationId,
            'userId' => $otherUserId,
            'token' => $jwtProvider->getJwt(),
        ]);
    }
}