<?php

namespace App\Controller\User;

use App\Entity\Conversation;
use App\Entity\User;
use App\Manager\ConversationManager;
use App\Manager\ParticipantManager;
use App\Manager\MessageManager;
use App\Repository\MessageRepository;
use App\Repository\UserRepository;
use App\Security\Voter\ConversationVoter;
use App\Service\Notifier;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * @Route("/message", name="message")
 */
class MessageController extends AbstractController
{
    /**
     * @Route("/send/{conversation}", methods={"POST"}, name="send-message", options={"expose"=true})
     */
    public function sendMessage(
        Request $request,
        MessageManager $messageManager,
        ConversationManager $conversationManager,
        Notifier $notifier,
        Conversation $conversation
    ): JsonResponse {
        if (!$request->isXmlHttpRequest()) {
            throw new AccessDeniedException();
        }

        $this->denyAccessUnlessGranted(ConversationVoter::OPEN, $conversation);

        $body = json_decode($request->getContent(), true);

        if (!array_key_exists('message', $body) || $body['message'] === '') {
            return $this->json(['succcess' => false]);
        }

        $isFirstConversationMessage = $conversation->getLastMessage() === null;

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        $message = $messageManager->create($conversation, $currentUser, $body['message'], false);
        $conversation->setLastMessage($message);
        $conversationManager->save($conversation);

        $view = $this->renderView('user/message.html.twig', [
            'message' => $message,
            'currentUserId' => $currentUser->getId(),
        ]);

        $lastMessageDateTime = $message
            ->getCreatedAt()
            ->setTimezone(new \DateTimeZone('Europe/Paris'));

        $notifier->newMessage($message, $currentUser, $conversation);

        return $this->json([
            'success' => true,
            'view' => $view,
            'lastMessageDatetime' => $lastMessageDateTime->format('d/m/Y H:i:s'),
            'isFirstConversationMessage' => $isFirstConversationMessage,
        ]);
    }

    /**
     * @Route("/{conversation}", name="get-conversation-messages", options={"expose"=true})
     */
    public function getConversationMessages(
        Request $request,
        MessageRepository $messageRepository,
        UserRepository $userRepository,
        Conversation $conversation
    ): JsonResponse {
        if (!$request->isXmlHttpRequest()) {
            throw new AccessDeniedException();
        }

        $this->denyAccessUnlessGranted(ConversationVoter::OPEN, $conversation);

        $currentUser = $this->getUser();
        $messages = $messageRepository->findByConversation($conversation->getId(), 15);
        $otherUser = $userRepository->findConversationOtherParticipantUser($conversation, $currentUser);

        $view = '<h3>Messages</h3>';

        if (!empty($messages)) {
            foreach ($messages as $message) {
                $view .= $this->renderView('user/message.html.twig', [
                    'message' => $message,
                    'currentUserId' => $currentUser->getId(),
                ]);
            }
        } else {
            $view .= 'Aucun message';
        }

        return $this->json([
            'userId' => $otherUser->getId(),
            'view' => $view,
        ]);
    }
}