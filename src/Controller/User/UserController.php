<?php

namespace App\Controller\User;

use App\Entity\User;
use App\Form\ChangePasswordType;
use App\Form\UserRegistrationType;
use App\Form\UserType;
use App\Manager\UserManager;
use App\Security\Voter\UserVoter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * @Route("/registration", name="user-registration")
     */
    public function registration(Request $request, UserManager $userManager): Response
    {
        $user = new User();
        $user->setPassword(uniqid('', true));

        $form = $this->createForm(UserRegistrationType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $userManager->save($user);

            $this->addFlash(
                'notice',
                'Votre compte a été créé. Veuillez consulter votre boîte mails.'
            );

            return $this->redirectToRoute('user-registration');
        }

        return $this->renderForm('user/registration.html.twig', [
            'form' => $form,
        ]);
    }

    /**
     * @Route("/user/edit", name="user-profile-edit")
     */
    public function editProfile(Request $request, UserManager $userManager): Response
    {
        $user = $this->getUser();

        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userManager->save($user);

            $this->addFlash(
                'notice',
                'Vos informations ont été mises à jour.'
            );

            return $this->redirectToRoute('user-profile', [
                'user' => $user->getId(),
            ]);
        }

        return $this->renderForm('user/profile-edit.html.twig', [
            'userForm' => $form,
            'userId' => $user->getId(),
        ]);
    }

    /**
     * @Route("/user/password", name="user-password-edit")
     */
    public function editPassword(
        Request $request,
        UserPasswordHasherInterface $passwordHasher,
        UserManager $userManager
    ): Response
    {
        $changePasswordForm = $this->createForm(ChangePasswordType::class);

        $changePasswordForm->handleRequest($request);
        if ($changePasswordForm->isSubmitted() && $changePasswordForm->isValid()) {
            /** @var User $user */
            $user = $this->getUser();

            $hashedPassword = $passwordHasher->hashPassword(
                $user,
                $changePasswordForm->get('new_password')->getData()
            );

            $user->setPassword($hashedPassword);
            $userManager->save($user);

            $this->addFlash(
                'notice',
                'Votre mot de passe a été correctement modifié.'
            );

            return $this->redirectToRoute('user-profile', [
                'user' => $user->getId(),
            ]);
        }

        return $this->renderForm('user/password-edit.html.twig', [
            'changePasswordForm' => $changePasswordForm,
            'userId' => $this->getUser()->getId(),
        ]);
    }

    /**
     * @Route("/user/{user}", name="user-profile", options={"expose"=true})
     */
    public function userProfile(Request $request, User $user): Response
    {
        $this->denyAccessUnlessGranted(UserVoter::VIEW, $user);

        if ($request->isXmlHttpRequest()) {
            $view = $this->renderView('user/profile-informations.html.twig', [
                'user' => $user,
            ]);

            return $this->json([
                'modalTitle' => 'Informations sur ' . $user->getUsername(),
                'view' => $view,
            ]);
        }

        return $this->render('user/profile.html.twig', [
            'user' => $user,
        ]);
    }
}
