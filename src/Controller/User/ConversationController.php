<?php

namespace App\Controller\User;

use App\Manager\ConversationManager;
use App\Repository\UserRepository;
use App\Service\Notifier;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ConversationController extends AbstractController
{
    /**
     * @Route(
     *     "/conversation/create/{username}",
     *     name="create-conversation",
     *     options={"expose"=true}
     * )
     */
    public function createConversation(
        Request $request,
        Notifier $notifier,
        UserRepository $userRepository,
        ConversationManager $conversationManager,
        string $username
    ): JsonResponse {
        if (!$request->isXmlHttpRequest()) {
            throw new AccessDeniedException();
        }

        $created = false;
        $message = 'L\'utilisateur "<b>' . $username . '</b>" n\'existe pas.';
        $view = null;
        $conversationId = null;

        $user = $userRepository->findByUsername($username);
        if ($user !== null) {
            $message = 'Une conversation existe déjà avec <b>' . $user->getUsername() . '</b>.';
            if ($user->getId() === $this->getUser()->getId()) {
                $message = 'Vous ne pouvez pas créer de conversation avec vous-même.';
            } else if (!$conversationManager->isConversationAlreadyExists($user)) {
                $conversation = $conversationManager->create($user);
                $conversationId = $conversation->getId();

                $created = true;
                $message = 'Conversation avec <b>' . $user->getUsername() . '</b> créée.';
                $view = $this->renderView('user/conversation.html.twig', [
                    'conversation' => [
                        'username' => $username,
                        'conversationId' => $conversationId,
                        'lastMessageContent' => null,
                        'lastMessageCreationDate' => null,
                    ],
                ]);

                $notifier->newConversation(
                    $user, $this->getUser()->getUserIdentifier(),
                    $conversationId
                );
            }
        }

        return $this->json([
            'created' => $created,
            'message' => $message,
            'view' => $view,
            'conversationId' => $conversationId,
        ]);
    }
}