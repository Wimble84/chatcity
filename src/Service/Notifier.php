<?php

namespace App\Service;

use App\Entity\Conversation;
use App\Entity\Message;
use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\Mercure\HubInterface;
use Symfony\Component\Mercure\Update;
use Twig\Environment;

class Notifier
{
    private HubInterface $hub;
    private Environment $tiwg;
    private UserRepository $userRepository;

    public function __construct(HubInterface $hub, Environment $tiwg, UserRepository $userRepository)
    {
        $this->hub = $hub;
        $this->tiwg = $tiwg;
        $this->userRepository = $userRepository;
    }

    public function newConversation(User $user, string $transmitterUsername, string $conversationId): void
    {
        $view = $this->tiwg->render('user/conversation.html.twig', [
            'conversation' => [
                'username' => $transmitterUsername,
                'conversationId' => $conversationId,
                'lastMessageContent' => null,
                'lastMessageCreationDate' => null,
            ],
        ]);

        $update = new Update(
            'new-conversation/' . $user->getId(),
            json_encode([
                'conversationView' => $view,
                'conversationId' => $conversationId,
            ]),
            true
        );

        $this->hub->publish($update);
    }

    public function newMessage(Message $message, User $currentUser, Conversation $conversation): void
    {
        $user = $this->userRepository->findConversationOtherParticipantUser($conversation, $currentUser);

        $view = $this->tiwg->render('user/message.html.twig', [
            'message' => $message,
            'fromMe' => true,
        ]);

        $update = new Update(
            'new-message/' . $user->getId(),
            json_encode([
                'messageView' => $view,
                'messageContent' => $message->getContent(),
                'conversationId' => $conversation->getId(),
                'messageDate' => ($message->getCreatedAt())->format('d/m/Y H:i:s')
            ]),
            true
        );
        $this->hub->publish($update);
    }
}
