<?php

namespace App\Service;

use App\Entity\User;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;

class Mailer
{
    private MailerInterface $mailer;

    private string $emailFrom;

    public function __construct(
        MailerInterface $mailer,
        string $emailFrom
    ) {
        $this->mailer = $mailer;
        $this->emailFrom = $emailFrom;
    }

    private function createAndSendMessage(string $subject, string $to, string $template, array $data): void
    {
        $email = new TemplatedEmail();
        $email
            ->from($this->emailFrom)
            ->to($to)
            ->subject($subject)
            ->htmlTemplate($template)
            ->context($data);

        $this->mailer->send($email);
    }

    public function userCreation(User $user): void
    {
        $this->createAndSendMessage(
            'ChatCity - Votre nouveau compte',
            $user->getEmail(),
            'email/user-creation.html.twig',
            [
                'firstname' => $user->getFirstname(),
                'lastname' => $user->getLastname(),
                'userIdentifier' => $user->getUserIdentifier(),
                'password' => $user->getPassword(),
            ]
        );
    }

}