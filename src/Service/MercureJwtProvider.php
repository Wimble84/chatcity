<?php

namespace App\Service;

use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key\InMemory;
use Symfony\Component\Mercure\Jwt\TokenProviderInterface;

class MercureJwtProvider implements TokenProviderInterface
{
    private string $secret;

    public function __construct(string $secret)
    {
        $this->secret = $secret;
    }

    public function getJwt(): string
    {
        $configuration = Configuration::forSymmetricSigner(
            new Sha256(),
            InMemory::plainText($this->secret)
        );

        return $configuration
            ->builder()
            ->withClaim('mercure', ['publish' => ['*'], 'subscribe' => ['*']])
            ->getToken(
                $configuration->signer(),
                $configuration->signingKey()
            )
            ->toString();
    }
}
