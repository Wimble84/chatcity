# ChatCity

Cloner le projet
```bash
$ git clone git@gitlab.com:Wimble84/chatcity.git
```

Builder et exécuter l'environnement **Docker**
```bash
$ docker-compose up -d --build
```

Installer les packages **Composer**
```bash
$ docker-compose run --rm php composer install
```

Installer les packages **Yarn**
```bash
$ docker-compose run --rm nodejs yarn install
```

Générer les assets JS/CSS
```bash
$ docker-compose run --rm nodejs yarn encore dev
```

Générer le fichier d'environnement de dev
```bash
$ docker-compose exec --rm php composer dump-env dev
```

Créer le schéma de base de données
```bash
$ docker-compose run --rm php bin/console d:m:mi
```

Injecter les fixtures dans la base de données
```bash
$ docker-compose run --rm php bin/console d:f:l
```

<br>

Se rendre à l'url suivante : [http://localhost](http://localhost)

****Identifiants****

- Utilisateurs :
    - **pierreb** : **pierreb**
    - **sophieb** : **sophieb**
    - **claireg** : **claireg**
    - **anneb** : **anneb**

<br>

Stopper les containers de l'environnement
```bash
$ docker-compose down
```