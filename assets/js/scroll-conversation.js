const scrollConversation = ($messages) => {
    $messages.scrollTop = $messages.scrollHeight;
}

export default scrollConversation;