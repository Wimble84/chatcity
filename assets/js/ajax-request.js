/* Ajax get request */
async function ajaxGet(requestUrl)
{
    let res = await fetch(requestUrl, {
        headers: {
            'Content-Type': 'application/json',
            'X-Requested-With': 'XMLHttpRequest'
        },
    });
    return await res.text();
}

async function ajaxPost(requestUrl, body)
{
    let res = await fetch(requestUrl, {
        method: 'post',
        headers: {
            'Content-Type': 'application/json',
            'X-Requested-With': 'XMLHttpRequest'
        },
        body: body
    });

    return res.json();
}

export { ajaxGet, ajaxPost };