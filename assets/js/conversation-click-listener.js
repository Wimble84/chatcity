import Routing from '../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';
import { ajaxGet } from './ajax-request';
import scrollConversation from './scroll-conversation';

const routes = require('../../public/js/fos_js_routes.json');
Routing.setRoutingData(routes);

const $messages = document.querySelector('#messages');

const conversationClickListener = (element) => {
    element.addEventListener('click', (e) => {
        const conversationId = e.currentTarget.dataset.id;

        if (!e.currentTarget.querySelector('.text-success').classList.contains('d-none')) {
            e.currentTarget.querySelector('.text-success').classList.add('d-none');
        }

        ajaxGet(Routing.generate('messageget-conversation-messages', { conversation: conversationId }))
            .then(result => {
                const data = JSON.parse(result);

                document.querySelectorAll('.conversation').forEach(element => {
                    element.classList.remove('bg-secondary', 'bg-opacity-25');
                });
                element.classList.add('bg-secondary', 'bg-opacity-25');
                document.querySelector('#new-message-input input[type=hidden]').value = conversationId;
                $messages.innerHTML = data.view;
                document.querySelector('#new-message-form button#view-profile').dataset.userId = data.userId;

                scrollConversation($messages);
            });
    })
};

export default conversationClickListener;