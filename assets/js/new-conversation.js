import Routing from '../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';
import conversationClickListener from './conversation-click-listener';
import { ajaxGet } from './ajax-request';

const routes = require('../../public/js/fos_js_routes.json');

Routing.setRoutingData(routes);

document
    .getElementById('add-conversation')
    .addEventListener('click', () => {
        const $alertSuccess = document.querySelector('#create-conversation-modal .alert.alert-success');
        const $alertDanger = document.querySelector('#create-conversation-modal .alert.alert-danger');
        const $usernameInput = document.querySelector('#search-user');

        if ($usernameInput.value !== '' && $usernameInput.value !== null) {
            if (!$alertSuccess.classList.contains('d-none')) {
                $alertSuccess.classList.add('d-none');
            }

            if (!$alertDanger.classList.contains('d-none')) {
                $alertDanger.classList.add('d-none');
            }

            ajaxGet(Routing.generate('create-conversation', { 'username': $usernameInput.value}))
                .then(result => {
                    const data = JSON.parse(result);
                    const $conversations = document.querySelector('#conversations');

                    $usernameInput.value = '';

                    let $alert = $alertDanger;
                    if (data.created) {
                        $alert = $alertSuccess;

                        const $noConversationMessage = $conversations.querySelector('#no-conversation-message');
                        if ($noConversationMessage !== null) {
                            $noConversationMessage.remove();
                        }

                        $conversations.insertAdjacentHTML('beforeend', data.view);

                        conversationClickListener(
                            document.querySelector('#conversation-' + data.conversationId)
                        );
                    }

                    $alert.innerHTML = data.message;
                    $alert.classList.remove('d-none');
                });
        }
});