const routes = require('../../public/js/fos_js_routes.json');
import Routing from '../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';
import { ajaxPost } from './ajax-request';

Routing.setRoutingData(routes);

const $newMessageForm = document.querySelector('#new-message-form');
const $messageInput = $newMessageForm.querySelector('#new-message-input input[type=text]');

const sendMessage = () => {
    const $messages = document.querySelector('#messages');
    const conversationId = $newMessageForm.querySelector('#new-message-input input[type=hidden]').value;
    let message = $messageInput.value;

    if (conversationId !== '' && conversationId !== null && message !== null && message !== '') {
        ajaxPost(
            Routing.generate('messagesend-message', { conversation: conversationId }),
            JSON.stringify({ message })
        ).then(result => {
            if (result.success) {
                if (result.isFirstConversationMessage) {
                    $messages.innerHTML = '<h3>Messages</h3>';
                }

                $messages.insertAdjacentHTML('beforeend', result.view);
                $messages.scrollTop = $messages.scrollHeight;
                $messageInput.value = '';

                if (message.length > 60) {
                    message = message.substr(0, 60) + '...';
                }

                document.querySelector('#conversation-' + conversationId + ' .last-message-content').innerHTML = message;
                document.querySelector('#conversation-' + conversationId + ' .last-message-date').innerHTML = result.lastMessageDatetime;
            }
        });
    }
};

$newMessageForm
    .querySelector('#new-message-send button#send')
    .addEventListener('click', sendMessage, false);

$messageInput.addEventListener('keyup', function (e) {
    if (e.code === 'Enter' || e.code === 'NumpadEnter') {
        sendMessage();
    }
});