import { EventSourcePolyfill } from 'event-source-polyfill';
import scrollConversation from "./scroll-conversation";

const url = JSON.parse(document.getElementById('receive-message-mercure-url').textContent);
const token = JSON.parse(document.getElementById('mercure-token').textContent);
const $messages = document.querySelector('#messages');

const eventSource = new EventSourcePolyfill(url, {
    headers: {
        'Authorization': 'Bearer ' + token,
    }
});

eventSource.onmessage = e => {
    const data = JSON.parse(e.data);
    const selectedConversation = document.querySelector('#new-message-form #new-message-input input[type=hidden]');
    const conversationId = data.conversationId;
    let messageContent = data.messageContent;

    if (messageContent.length > 60) {
        messageContent = messageContent.substr(0, 60) + '...';
    }

    if (conversationId !== selectedConversation.value) {
        document.querySelector('#conversation-' + conversationId + ' .text-success').classList.remove('d-none');
    } else {
        $messages.insertAdjacentHTML('beforeend', data.messageView)
        scrollConversation($messages);
    }

    document.querySelector('#conversation-' + conversationId + ' .last-message-content').innerHTML = messageContent;
    document.querySelector('#conversation-' + conversationId + ' .last-message-date').innerHTML = data.messageDate;


};