import Routing from '../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';
import { ajaxGet } from './ajax-request';
import { Modal } from 'bootstrap';

const routes = require('../../public/js/fos_js_routes.json');
Routing.setRoutingData(routes);

const $newMessageForm = document.querySelector('#new-message-form');
const $userProfileModal = document.querySelector('#user-profile-modal');

$newMessageForm
    .querySelector('#new-message-send button#view-profile')
    .addEventListener('click', e => {
        const userId = e.currentTarget.dataset.userId;

        ajaxGet(Routing.generate('user-profile', { user: userId }))
            .then(result => {
                const data = JSON.parse(result);

                $userProfileModal.querySelector('.modal-title').innerHTML = data.modalTitle;
                $userProfileModal.querySelector('.modal-body').innerHTML = data.view;
                (new Modal($userProfileModal)).show();
            });
    });