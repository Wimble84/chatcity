import '../styles/conversations.scss';

import { EventSourcePolyfill } from 'event-source-polyfill';
import conversationClickListener from "./conversation-click-listener";
import scrollConversation from "./scroll-conversation";

const url = JSON.parse(document.getElementById('new-conversation-mercure-url').textContent);
const token = JSON.parse(document.getElementById('mercure-token').textContent);

scrollConversation(document.querySelector('#messages'));

document.querySelectorAll('.conversation').forEach($conversation => {
    conversationClickListener($conversation);
});

const $createConversationModal = document.querySelector('#create-conversation-modal');
$createConversationModal.addEventListener('hide.bs.modal', () => {
    const $alertSuccess = $createConversationModal.querySelector('.alert.alert-success');
    const $alertDanger = $createConversationModal.querySelector('.alert.alert-danger');

    if (!$alertSuccess.classList.contains('d-none')) {
        $alertSuccess.classList.add('d-none');
    }

    if (!$alertDanger.classList.contains('d-none')) {
        $alertDanger.classList.add('d-none');
    }
});

const eventSource = new EventSourcePolyfill(url, {
    headers: {
        'Authorization': 'Bearer ' + token,
    }
});

eventSource.onmessage = e => {
    const data = JSON.parse(e.data);
    const $conversations = document.querySelector('#conversations');
    const $noConversationMessage = $conversations.querySelector('#no-conversation-message');

    if ($noConversationMessage !== null) {
        $noConversationMessage.remove();
    }

    $conversations.insertAdjacentHTML('beforeend', data.conversationView);

    conversationClickListener(
        document.querySelector('#conversation-' + data.conversationId)
    );
};